package stapler.model;

public class MainClass {

	public static void main(String[] args) {
		
		Stapler myStapler = new Stapler("black", 80);
		
		System.out.println(myStapler.getCapacity());
		myStapler.load(80);
		
		while(myStapler.isLoaded()) {
			//System.out.println(myStapler.getCurrentLoad());
			myStapler.useOnce();
		}
	}
}
/*
 * The model design of this Stapler can be used in creating a basic copy of the object into the virtual world. It provides the most common
 * attributes and procedures that replicate the real-world model. 
 */
