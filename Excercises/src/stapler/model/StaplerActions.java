package stapler.model;

public interface StaplerActions {
	void useOnce();
	void load(int sizeOfLoad);
}
