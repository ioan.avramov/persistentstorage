package stapler.model;

public class Stapler implements StaplerActions{
	
	private String color;
	private int capacity;
	private int currentLoad;
	private boolean isLoaded = false;
	
	public Stapler(String color, int capacity) {
		this.color = color;
		this.capacity = capacity;
	}
	
	public String getColor() {
		return color;
	}
	public int getCapacity() {
		return capacity;
	}
	public boolean isLoaded() {
		return isLoaded;
	}
	public int getCurrentLoad() {
		return currentLoad;
	}
	@Override
	public void useOnce() {
		if(isLoaded) {
			currentLoad--; // Simulates usage. 
			checkLoad();
		}
	}
	@Override
	public void load(int sizeOfLoad) {
		if(sizeOfLoad <= capacity) {
			this.currentLoad = sizeOfLoad;
			this.isLoaded = true;
		}
		else{
			System.out.println("Cannot load more than it's capacity.");
		}
	}
	
	private void checkLoad() {
		if(currentLoad == 0) {
			isLoaded=false;
			System.out.println("Empty.");
		}
			
	}


	
}
