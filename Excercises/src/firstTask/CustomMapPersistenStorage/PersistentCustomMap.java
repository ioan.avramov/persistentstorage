package firstTask.CustomMapPersistenStorage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

public class PersistentCustomMap {

	public static void main(String[] args) {
		
		MyMap map = new MyMap();
		
		try {
			
			map = loadExternalData();
		}
		catch(Exception e){
			e.printStackTrace();
			if(e instanceof FileNotFoundException)
			{
				populateMap(map);
			}
		}
		
		System.out.println(map.remove("two")); // testing 
		
		try {
			saveToDrive(map);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	private static void populateMap(MyMap<String,Integer> map) {
			map.put("one",1);
	        map.put("two",2);
	        map.put("three",3);
	}
	
	private static MyMap<Object,Object> loadExternalData() throws IOException, ClassNotFoundException {
		 	FileInputStream fis = new FileInputStream("map1.ser");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        MyMap newMap = (MyMap) ois.readObject();
	        ois.close();
	        return newMap;
	}
	private static boolean saveToDrive(MyMap map) throws IOException {
		
		FileOutputStream fos = new FileOutputStream("map1.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
		return true;
	}

}
