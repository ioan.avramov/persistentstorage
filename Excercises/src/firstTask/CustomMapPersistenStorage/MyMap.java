package firstTask.CustomMapPersistenStorage;

import java.io.Serializable;

public class MyMap<K,V> implements Serializable{

	
	private Pair<K, V> mapList[] = new Pair[100];
	private int pos = 0;
	
	public V get(K key) {
		for(int i = 0; i<mapList.length; i++) {
			
			if(mapList[i] != null && mapList[i].getKey().equals(key)) {
				return mapList[i].getValue();
			}
		}
		return null;
	}
	 
	public void put(K key, V value) {
	   if(pos >= mapList.length) {
		  // increase the length of the array
	   }
	   if(!this.contains(key)) {
		   Pair<K,V> curr = new Pair<K, V>(key, value);
		   mapList[pos] = curr;
		   pos++;
	   }
	   else{
		   System.out.println("This key already exists");
	   }
	   
	}
	     
	public boolean remove(K key) {
		for(int i = 0; i< mapList.length; i++) {
			if(mapList[i] != null && mapList[i].getKey().equals(key)){
				mapList[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public boolean contains(K key) {
		for(int i = 0; i< mapList.length; i++) {
			if(mapList[i] != null && mapList[i].getKey().equals(key)){
				return true;
			}
		}
		return false;
	}
}

