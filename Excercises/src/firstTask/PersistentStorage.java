package firstTask;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
public class PersistentStorage {

	public static void main(String[] args) {
			
		Map map = new HashMap();
		try {
			
			map = loadExternalData();
		}
		catch(Exception e){
			e.printStackTrace();
			if(e instanceof FileNotFoundException)
			{
				populateMap(map);
			}
		}
		
		for(Object key: map.keySet()){
			System.out.println(map.get(key));
		}
		
		
		
		try {
			saveToDrive(map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void populateMap(Map<Integer,String> map) {
			map.put(1,"one");
	        map.put(2,"two");
	        map.put(3,"three");
	}
	
	private static Map<Object,Object> loadExternalData() throws IOException, ClassNotFoundException {
		 	FileInputStream fis = new FileInputStream("map.ser");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        Map newMap = (Map) ois.readObject();
	        ois.close();
	        return newMap;
	}
	private static boolean saveToDrive(Map map) throws IOException {
		
		FileOutputStream fos = new FileOutputStream("map.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
		return true;
	}

}
